# Install Dockerized Version of IBM DB2

## Create a bridged network
- `docker network create --driver bridge my_nw`

## Install DB2
- `docker run -itd --name mydb2 --network=my_nw --privileged=true -p 50000:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=bibao -e DBNAME=LOCDB -v c:/database ibmcom/db2`

## Configure DB2 in DBeaver
- Add a new DB2 LUW connection
- Connect with the following parameters
	- Url: `jdbc:db2://localhost:50000/LOCDB`
	- Host: localhost
	- Port: 50000
	- Database: LOCDB
	- Username: db2inst1
	- Password: bibao
	
## Move db2 driver to maven repository
- Copy db2jcc4.jar and db2jcc_license_cisuz.jar to a folder, say c:\OpenSource\jdbc
- Run the following commands
	- `mvn install:install-file -DgroupId=com.ibm.db2.jcc -DartifactId=db2jcc4 -Dversion=4.25.23 -Dpackaging=jar -Dfile=c:/opensource/jdbc/db2jcc4.jar -DgeneratePom=true`
	- `mvn install:install-file -DgroupId=com.ibm.db2.jcc -DartifactId=db2jcc_license_cisuz -Dversion=4.25.23 -Dpackaging=jar -Dfile=c:/opensource/jdbc/db2jcc_license_cisuz.jar -DgeneratePom=true`
- Maven Dependencies for db2

		<!-- DB2 -->
		<dependency>
			<groupId>com.ibm.db2.jcc</groupId>
			<artifactId>db2jcc4</artifactId>
			<version>4.25.23</version>
		</dependency>
		
		<dependency>
			<groupId>com.ibm.db2.jcc</groupId>
			<artifactId>db2jcc_license_cisuz</artifactId>
			<version>4.25.23</version>
		</dependency>
