# Install KIND, Docker, Helm

## Preparation:
#### 1. Go to https://console.cloud.google.com/gcr/images/kubernetes-helm/GLOBAL/tiller and export its certificate to a file, then install it to windows store. 
#### 2. Copy tiller.yaml and service-account-tiller.yaml to an empty folder, say c:\docker
#### 3. Start Windows PowerShell at admin level. Go to folder c:\docker

## Step 1: Install Chocolaty
- `Set-ExecutionPolicy Bypass -Scope Process -Force; iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex`

## Step 2: Install kind (Always answer "Yes" during installation)
- `choco install kubernetes-cli # Install latest kubectl`
- `choco install kubernetes-helm --version=2.13.0 # Install helm 2 for helm update using tiller server`
- `choco install kind`
#### Note: You may need to restart PowerShell to make the updated path available. 

## Step 3: Launch Docker Desktop

## Step 4: User KIND to build cluster
- `kind create cluster --name bibao`
- `kind get clusters`
- `kubectl cluster-info --context kind-bibao`
- `kubectl get pods --all-namespaces`

## Step 5: Install tiller
- `kubectl create -f service-account-tiller.yaml`
- `kubectl apply -f tiller.yaml`
#### Note: alternative image: sapcc/tiller:v2.14.3
- `kubectl get pods --all-namespaces`

## Step 6: Install local kubernetes dashboard (Optional)
- `helm version`
- `helm init`
- `helm repo update`
- `helm install --name heartbeat stable/heartbeat`
- `kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml`

## Step 7: Start local kubernetes dashboard
- `kubectl -n kube-system get secret`
- `kubectl -n kube-system describe secret <GET POD NAME STARTS WITH deployment-controller-token>`
#### Copy the token from the output of above command and it will be used for the following login.
- `kubectl proxy`
#### Open browser and input http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login

