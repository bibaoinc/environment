# Install Sonarqube

## Run command line (You may need a few minutes until sonarqube server starts)
- `docker run --restart=always -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube:alpine`

## Open browser and input http://localhost:9000/projects

## Run sonar scan for local projects
- `mvn clean test sonar:sonar`